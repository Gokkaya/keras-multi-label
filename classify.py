# -*- coding: utf-8 -*-

from keras.preprocessing.image import img_to_array
from keras.models import load_model
import numpy as np
import argparse
import imutils
import pickle
import cv2
import os
# argumanlari ayristirdirdik
ap = argparse.ArgumentParser()
# model yolunu ekledik
ap.add_argument("-m", "--model", required=True,help="path to trained model model")
# labellarin yolunu ekledik  
ap.add_argument("-l", "--labelbin", required=True,help="path to label binarizer")
# resim yolunu belirttik
ap.add_argument("-i", "--image", required=True,help="path to input image")
args = vars(ap.parse_args())

# resim yukleniyor
image = cv2.imread(args["image"])
output = imutils.resize(image, width=400)
 
# Resmi siniflandirma icin on islemler yapiliyor

# resim boyutlandirildi
image = cv2.resize(image, (96, 96))
# toplam dataset buyuklugu
image = image.astype("float") / 255.0
image = img_to_array(image)
image = np.expand_dims(image, axis=0)

# egitimli konvolusyonel noral agi ve coklu etiketi yukluyoruz
print("[INFO]  network yokleniyor...")
model = load_model(args["model"])
# resimler ve kategoriler binary olarak eslestiriliyor
mlb = pickle.loads(open(args["labelbin"], "rb").read())

# giris goruntusunu siniflandirin ve iki sinifin dizinlerini bulun
# en buyuk olasilikla olan iki etiket
print("[INFO] resim siniflandiriliyor...")
proba = model.predict(image)[0]
idxs = np.argsort(proba)[::-1][:2]
# resim OpenCV vasitasiyla kullanniciya gosteriliyor
for (i, j) in enumerate(idxs):
	label = "{}: {:.2f}%".format(mlb.classes_[j], proba[j] * 100)
	cv2.putText(output, label, (10, (i * 30) + 25), 
		cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)

# Her bir label icin olasiliklari goster

for (label, p) in zip(mlb.classes_, proba):
	print("{}: {:.2f}%".format(label, p * 100))

# resmi show et
cv2.imshow("Output", output)
cv2.waitKey(0)