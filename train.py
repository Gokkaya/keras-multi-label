# -*- coding: utf-8 -*-

import matplotlib
matplotlib.use("Agg")

from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
from keras.preprocessing.image import img_to_array
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.model_selection import train_test_split
from multiclass_model.multi_label_model import SmallerVGGNet
import matplotlib.pyplot as plt
from imutils import paths
import numpy as np
import argparse
import random
import pickle
import cv2
import os

# argumanlari ayristirdirdik
ap = argparse.ArgumentParser()
# dataset yolunu ekledik
ap.add_argument("-d", "--dataset", required=True,
	help="path to input dataset (i.e., directory of images)")
# model yolunu ekledik
ap.add_argument("-m", "--model", required=True,
	help="path to output model")
# labellarin yolunu ekledik  
ap.add_argument("-l", "--labelbin", required=True,
	help="path to output label binarizer")
# plot vasitasiyla grafik cizecegiz
ap.add_argument("-p", "--plot", type=str, default="plot.png",
	help="path to output accuracy/loss plot")
args = vars(ap.parse_args())

# İlk öğrenim hızını öğrenmek için eğitilecek çağların sayısını başlat
# parti boyutu ve resim boyutları
# periyod
EPOCHS = 150  
INIT_LR = 1e-3
BS = 32
IMAGE_DIMS = (96, 96, 3)


# Resim yollarını yakala ve rastgele karıştır
print("[INFO] resim yukleniyor...")
imagePaths = sorted(list(paths.list_images(args["dataset"])))
print imagePaths
random.seed(42)
random.shuffle(imagePaths)

# verileri ve etiketleri başlat
data = []
labels = []

# giriş görüntüleri üzerinde döngü
for imagePath in imagePaths:
	# görüntüyü yükleyin, önceden işleyin ve veri listesine kaydedin
	image = cv2.imread(imagePath)
	image = cv2.resize(image, (IMAGE_DIMS[1], IMAGE_DIMS[0]))
	image = img_to_array(image)
	data.append(image)

	# resim yolundan sınıf etiketlerinin çıkarılması ve
	# etiket listesi
	l = label = imagePath.split(os.path.sep)[-2].split("_")
	labels.append(l)

# ham piksel yoğunluklarını aralığa ölçeklendirin [0, 1]
data = np.array(data, dtype="float") / 255.0
labels = np.array(labels)
print("[INFO] data matrix: {} images ({:.2f}MB)".format(
	len(imagePaths), data.nbytes / (1024 * 1000.0)))

# scikit-learn'in özel çoklu etiketini kullanarak etiketleri ikili hale getirin
# binarizer uygulaması
print("[INFO] labels siniflari:")
mlb = MultiLabelBinarizer()
labels = mlb.fit_transform(labels)

# sınıf etiketlerinin her birini döngüyle gez ve bunları göster
for (i, label) in enumerate(mlb.classes_):
	print("{}. {}".format(i + 1, label))

# verilerin %80i eğitim %20 si test için ayrıldı
(trainX, testX, trainY, testY) = train_test_split(data,
	labels, test_size=0.2, random_state=42)

# veri augmenta için görüntü oluşturucu oluştur
aug = ImageDataGenerator(rotation_range=25, width_shift_range=0.1,
	height_shift_range=0.1, shear_range=0.2, zoom_range=0.2,
	horizontal_flip=True, fill_mode="nearest")

# modeli son katman olarak bir sigmoid aktivasyonu kullanarak başlat
# ağda çok etiketli sınıflandırma yapabiliriz
print("[INFO] model derleniyor...")
model = SmallerVGGNet.build(
	width=IMAGE_DIMS[1], height=IMAGE_DIMS[0],
	depth=IMAGE_DIMS[2], classes=len(mlb.classes_),
	finalAct="sigmoid")

# optimize ediciyi başlat 
opt = Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS)
model.compile(loss="binary_crossentropy", optimizer=opt,
	metrics=["accuracy"])

# network eğitiliyor
H = model.fit_generator(
	aug.flow(trainX, trainY, batch_size=BS),
	validation_data=(testX, testY),
	steps_per_epoch=len(trainX) // BS,
	epochs=EPOCHS, verbose=1)

# model diske kaydediliyor
model.save(args["model"])

# Çok etiketli binarizörü diske kaydetme
f = open(args["labelbin"], "wb")
f.write(pickle.dumps(mlb))
f.close()

# plot ile loss acc grafiği çiziliyor
plt.style.use("ggplot")
plt.figure()
N = EPOCHS
plt.plot(np.arange(0, N), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, N), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, N), H.history["acc"], label="train_acc")
plt.plot(np.arange(0, N), H.history["val_acc"], label="val_acc")
plt.title("Training Loss and Accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="upper left")
plt.savefig(args["plot"])